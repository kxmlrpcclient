# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sairan Kikkarin <sairan@computer.org>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-14 00:14+0000\n"
"PO-Revision-Date: 2011-02-22 01:45+0600\n"
"Last-Translator: Sairan Kikkarin <sairan@computer.org>\n"
"Language-Team: Kazakh <kde-i18n-doc@kde.org>\n"
"Language: kk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: query.cpp:253
#, kde-format
msgid "Received invalid XML markup: %1 at %2:%3"
msgstr "%1 деген XML белгісі дұрыс емес: %2:%3"

#: query.cpp:266
#, kde-format
msgid "Unknown type of XML markup received"
msgstr "XML белгісінің беймәлім түрі кездесті"
