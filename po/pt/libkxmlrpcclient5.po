# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: libkxmlrpcclient5\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-14 00:14+0000\n"
"PO-Revision-Date: 2015-02-12 10:45+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: query.cpp:253
#, kde-format
msgid "Received invalid XML markup: %1 at %2:%3"
msgstr "Recebidas marcas XML inválidas: %1 em %2:%3"

#: query.cpp:266
#, kde-format
msgid "Unknown type of XML markup received"
msgstr "Foi recebido um tipo de marca de XML desconhecido"
